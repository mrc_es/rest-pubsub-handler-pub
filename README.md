# Rest Pub/Sub handler.

Crea un usuario, una cuenta de servicio y un access token, para utilizar Pub Sub de Google Cloud Platform. Después le hace pull a un mensaje.

### Modo de uso.

Las variables necesarias para su uso, son:

`<PROJECT_ID>`

`<ACCESS_TOKEN>`

`<SUBSCRIPTION_NAME>`

Se tienen que configurar dentro del script.

En caso de querer crear un *access token*, entonces configurar las variables

`<NEW_SERVICE_ACCOUNT_NAME>`

`<JSON_SERVICE_ACCOUNT_PATH>`

dentro de la función `create_access_token`.

Ejecutar el código de la forma.

```shell
./main.sh [--create-access-token]
```

donde el parámetro `--create-access-token` permite la creación de un token de acceso en caso de no contar con uno.