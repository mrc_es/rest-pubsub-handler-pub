#!/bin/bash

: << __info__
    Autor: Marco Cuauhtli Elizalde Sánchez.
__info__

#[START] Constantes de configuración.
declare -r _PROJECT='<PROJECT_ID>'
export _ACCESS_TOKEN='<ACCESS_TOKEN>'
declare -r _SUBSCRIPTION_NAME='<SUBSCRIPTION_NAME>'  # Sólo el nombre de la subscripción, no la ruta.
#[END] Constantes de configuración.

export _SUBSCRIPTION="projects/${_PROJECT}/subscriptions/${_SUBSCRIPTION_NAME}"
export _URL="https://pubsub.googleapis.com/v1/${_SUBSCRIPTION}"
export _PROJECT

alias _nul='/dev/null'
shopt -s expand_aliases

log() {

    while [[ $# > 0 ]]
    do
        case "$1" in 
            -l | --level)
                shift
                declare -u _log_level="$1"
                shift
            ;;
            -n | --message_location )
                shift
                declare _message_location="$1"
                shift
            ;;
            -m | --message )
                shift
                declare _message="$1"
                shift
            ;;
            [^\^] )
                shift
                    [[ -z "${_message+x}" ]] || declare _message="$1"
                shift
            ;;
            * )
                shift
            ;;
        esac
    done

    declare -u _log_level="${_log_level:-"INFO"}"

    printf '%s|%s|%s:%s\n' "$_log_level" "$(date +'%Y/%m/%dT%H:%M:%S')" "$_message_location" "$_message"
}

check_settings() {
    declare _PROJECT_NAME=$(gcloud config list --format 'value(core.project)' 2> _null)
    declare _ACCOUNT_NAME=$(gcloud config list --format 'value(core.account)' 2> _null)

    while :
    do
        printf \
            'Usar\n  Proyecto: %s\n  Cuenta: %s\ncomo projecto y cuenta respectivamente? (s|S|y|Y|n|N): ' \
            "$_PROJECT_NAME" \
            "$_ACCOUNT_NAME"

        declare -l respuesta
        read respuesta

        if [[ "$respuesta" =~ ^n$ ]]
        then
            exit
        elif [[ "$respuesta" =~ ^(y|s)$ ]]
        then
            break
        else
            echo ""
            echo "Contesta bien."
            echo ""
        fi
    done
}

create_and_bind_publisher() {

    log \
        --level info \
        --message_location "${FUNCNAME}" \
        --message "Creando service account \"${_PUBLISHER_NAME}\""

    gcloud iam \
        service-accounts \
        create "$_PUBLISHER_NAME" > _null

    gcloud projects \
        add-iam-policy-binding \
        "$_PROJECT" \
        --member="serviceAccount:${_SERVICE_ACCOUNT}" \
        --role=roles/pubsub.publisher > _null
}

create_access_token() {

    #[START] Constantes de configuración.
    declare -r _PUBLISHER_NAME='<NEW_SERVICE_ACCOUNT_NAME>'  #  Nombre de la cuenta de servicio
    declare _KEY_FILE='<JSON_SERVICE_ACCOUNT_PATH>'  #  Ruta de la service account. Si no existe,
                                                     #+ se crea más adelante en esa localización.
    #[END] Constantes de configuración.    
    
    declare _SERVICE_ACCOUNT="${_PUBLISHER_NAME}@${_PROJECT}.iam.gserviceaccount.com"
    export _SERVICE_ACCOUNT
    export _PUBLISHER_NAME

    check_settings
    create_and_bind_publisher

    if [[ ! -s "$_KEY_FILE" ]]
    then
        log \
            --level info \
            --message_location "${FUNCNAME}" \
            --message "Creando key-file en: \"${_KEY_FILE}\""
        
        gcloud iam \
                service-accounts \
                keys \
                create "$_KEY_FILE" \
                --iam-account="$_SERVICE_ACCOUNT"
    fi
    
    log \
        --level info \
        --message_location "${FUNCNAME}" \
        --message "Usando key-file: \"${_KEY_FILE}\""

    log \
        --level info \
        --message_location "${FUNCNAME}" \
        --message "Activando service account: \"${_SERVICE_ACCOUNT}\""

    gcloud auth \
        activate-service-account \
        "$_SERVICE_ACCOUNT" \
        --key-file="$_KEY_FILE"


    _ACCESS_TOKEN="$(
        gcloud auth application-default print-access-token
    )"

    log \
        --level info \
        --message_location "${FUNCNAME}" \
        --message "Usando el access token: \"${_ACCESS_TOKEN}\""
}

pull_message() {

    curl \
        --silent \
        --request POST \
        --header "Authorization: Bearer ${_ACCESS_TOKEN}" \
        --header "Content-Type: application/json" \
        --data '{"maxMessages":1}' \
        --url "${_URL}:pull"
}

get_parameters() {

    while [[ $# > 0 ]]
    do
        case "$1" in
            --create-access-token )
                shift
                export create_access_token=0
                ;;
            * )
                shift
                ;;
        esac
    done
}

main() {

    #  Dentro de la función create_access_token modificar 
    #+ las constantes de configuración.
    get_parameters $@
    [[ "${create_access_token+x}" ]] && create_access_token
    pull_message
}

main
